const express = require('express');
const moment = require('moment');
const router = express.Router();

const kT2 = require(`../k''T.json`);
const data = require('../allData.json');
const table8 = require('../table8.json');

var values_from_algorithm4 = [];

var WEc_Val = {
    "SO2_Sensor_id" : "a0f217ede1bb25f89e5fbb8ad7584061",
    values_from_algorithm4
};

router.get('/', function (req, res) {
    // console.log(data.length);s
    for(var i = 0; i < data.length; i++){
        // For calculating Outdoor data
        var WEu = data[i].OPT1_SO2;
        var AEu = data[i].OPT2_SO2;
        // console.log(WEu);
        // console.log(AEu);

        var WEe = table8.Electronic_Zero_WEe;
        var AEe = table8.Electronic_Zero_AEe;
        // console.log(WEe);
        // console.log(AEe);

        var WET = JSON.parse(table8.WE_Zero_WET);
        var AET = JSON.parse(table8.Aux_Zero_AET);

        // console.log(WEo, "", AEo);

        var temp = data[i].Temp_in_oC;
        temp = JSON.parse(temp.toFixed(1));
        // console.log(temp);

        var factor_kT2 = kT2[`${temp}`];

        var WEo = WET - WEe;
        var AEo = AET - AEe;
        // console.log(WEo, " ", AEo);

        var WEc = (WEu - WEe) - WEo - factor_kT2;
        WEc = parseFloat(WEc.toFixed(3));
        // console.log(WEc);

        var gain = (WEc * table8.Gain);
        gain = parseFloat(gain.toFixed(3));

        var WEc_obj = {
            ...data[i],
            "WEc" : WEc,
            "Gain_Value" : gain
        }
        values_from_algorithm4.push(WEc_obj);
    }
    console.log(WEc_Val);
    res.status(200).send(WEc_Val);
});

router.get('/', function (req, res) {
    // console.log(data.length);
    for(var i = 0; i < data.length; i++){
        // For calculating Outdoor data
        var WEu = data[i].OPT1_SO2;
        var AEu = data[i].OPT2_SO2;
        // console.log(WEu);
        // console.log(AEu);

        var WEe = table8.Electronic_Zero_WEe;
        var AEe = table8.Electronic_Zero_AEe;
        // console.log(WEe);
        // console.log(AEe);

        var WET = JSON.parse(table8.WE_Zero_WET);
        var AET = JSON.parse(table8.Aux_Zero_AET);

        // console.log(WEo, "", AEo);

        var temp = data[i].Temp_in_oC;
        temp = JSON.parse(temp.toFixed(1));
        // console.log(temp);

        var factor_kT1 = kT1[`${temp}`];

        var WEo = WET - WEe;
        var AEo = AET - AEe;
        // console.log(WEo, " ", AEo);

        // For calculating WEc
        var diff = WEo - AEo;
        // console.log(div);

        // var t = kT1 * div;
        // console.log(t);

        var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);
        WEc = parseFloat(WEc.toFixed(3));
        // console.log(WEc);

        var gain = (WEc * table8.Gain);
        gain = parseFloat(gain.toFixed(3));

        var WEc_obj = {
            ...data[i],
            "WEc" : WEc,
            "Gain_Value" : gain
        }
        values_from_algorithm3.push(WEc_obj);
    }
    console.log(WEc_Val);
    res.status(200).send(WEc_Val);
});

router.get("/date", (req, res) => {
    var sensor_id = req.query.sensor_id;

    if(sensor_id === "a0f217ede1bb25f89e5fbb8ad7584061"){
        var from_date = moment(req.query.from_date, 'YYYY-MM-DD HH:mm:ss');
        // console.log(from_date);
        var to_date = moment(req.query.to_date, 'YYYY-MM-DD HH:mm:ss');
        // console.log(to_date);

        values_from_algorithm4 = [];

        var all = {
            "SO2_Sensor_id" : "a0f217ede1bb25f89e5fbb8ad7584061",
            values_from_algorithm4
        };

        var WEe = table8.Electronic_Zero_WEe;
        var AEe = table8.Electronic_Zero_AEe;
        // console.log(WEe);
        // console.log(AEe);

        var WET = JSON.parse(table8.WE_Zero_WET);
        var AET = JSON.parse(table8.Aux_Zero_AET);

        for(var i = 0; i < data.length; i++){
            var time_stamp = data[i].Time_Stamp;
            // console.log(time_stamp);
            time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

            // console.log(time_stamp);
            if(req.query.from_date === "" && req.query.to_date === ""){
                var WEu = data[i].OPT1_SO2;
                var AEu = data[i].OPT2_SO2;
                // console.log(WEu);
                // console.log(AEu);

                var temp = data[i].Temp_in_oC;
                temp = JSON.parse(temp.toFixed(1));
                // // console.log(temp);

                var factor_kT2 = kT2[`${temp}`];
                // // console.log(factor_nT);

                var WEo = WET - WEe;
                var AEo = AET - AEe;
                // console.log(WEo, " ", AEo);

                // For calculating WEc
        
                var WEc = (WEu - WEe) - WEo - factor_kT2;

                WEc = parseFloat(WEc.toFixed(3));
                // // console.log(WEc);

                var ppb = (WEc / table8.Sensitivity);
                ppb = ppb.toFixed(3)

                var gain = (WEc * table8.Gain);
                gain = parseFloat(gain.toFixed(3));

                var WEc_obj = {
                    ...data[i],
                    "WEc" : WEc,
                    "PPB" : ppb,
                    "Gain_Value" : gain
                }
                values_from_algorithm4.push(WEc_obj);
            }

            if(time_stamp >= from_date && time_stamp <= to_date) {

                var WEu = data[i].OPT1_SO2;
                var AEu = data[i].OPT2_SO2;
                // console.log(WEu);
                // console.log(AEu);

                var temp = data[i].Temp_in_oC;
                temp = JSON.parse(temp.toFixed(1));
                // // console.log(temp);

                var factor_kT2 = kT2[`${temp}`];
                // // console.log(factor_nT);

                var WEo = WET - WEe;
                var AEo = AET - AEe;
                // console.log(WEo, " ", AEo);

                // For calculating WEc
        
                var WEc = (WEu - WEe) - WEo - factor_kT2;

                WEc = parseFloat(WEc.toFixed(3));
                // // console.log(WEc);

                var ppb = (WEc / table8.Sensitivity);
                ppb = ppb.toFixed(3)

                var gain = (WEc * table8.Gain);
                gain = parseFloat(gain.toFixed(3));

                var WEc_obj = {
                    ...data[i],
                    "WEc" : WEc,
                    "PPB" : ppb,
                    "Gain_Value" : gain
                }
                values_from_algorithm4.push(WEc_obj);
            }
        }
        res.status(200).send(all);
    }else{
        res.status(400).send({"Message" : "Proper sensor_id is needed"});
    }
});

module.exports = router;