const express = require('express');
const moment = require('moment');
const axios = require('axios');

const router = express.Router();

const nT = require('../nT.json');
const kT = require('../kT.json');
const kT1 = require(`../k'T.json`);
const kT2 = require(`../k''T.json`);
const data = require('../allData.json');
const table8 = require('../table8.json');
const table9 = require('../table9.json');

router.post('/', function (req, res){
    var algo = req.body.algorithm;
    var sensor_name = req.body.sensor_name;

    var from_date = moment(req.body.from_date, 'YYYY-MM-DD HH:mm:ss');
    var to_date = moment(req.body.to_date, 'YYYY-MM-DD HH:mm:ss');

    var WEe = table8.Electronic_Zero_WEe;
    var AEe = table8.Electronic_Zero_AEe;

    // If Algorithm 1
    if(algo === 1){
        var values = [];

        var obj = {
            "sensor_name" : sensor_name,
            values
        };

        if(sensor_name === "SO2"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].Time_Stamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    var WEu = data[i].OPT1_SO2;
                    var AEu = data[i].OPT2_SO2;
    
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_nT = nT[`${temp}`];
    
                    // // For calculating WEc
                    var WEc = (WEu - WEe) - factor_nT * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    var WEu = data[i].OPT1_SO2;
                    var AEu = data[i].OPT2_SO2;
                
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_nT = nT[`${temp}`];
    
                    // // For calculating WEc using Algorithm 1
                    var WEc = (WEu - WEe) - factor_nT * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);
            
        }else if(sensor_name === "NO2"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].Time_Stamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    var WEu = data[i].OPT1_NO2;
                    var AEu = data[i].OPT2_NO2;
    
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_nT = nT[`${temp}`];
    
                    // // For calculating WEc
                    var WEc = (WEu - WEe) - factor_nT * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    var WEu = data[i].OPT1_NO2;
                    var AEu = data[i].OPT2_NO2;
                
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_nT = nT[`${temp}`];
    
                    // // For calculating WEc using Algorithm 1
                    var WEc = (WEu - WEe) - factor_nT * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);
            
        }else{
            res.status(200).send({"Message" : `This sensor ${sensor_name} is not available right now!`});
        }
    }
    // If Algorithm 2
    else if(algo === 2){
        // console.log("Algo 2 is selected");
        var values = [];

        var WET = JSON.parse(table8.WE_Zero_WET);
        var AET = JSON.parse(table8.Aux_Zero_AET);

        var obj = {
            "sensor_name" : sensor_name,
            values
        };

        if(sensor_name === "SO2"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].Time_Stamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    var WEu = data[i].OPT1_SO2;
                    var AEu = data[i].OPT2_SO2;
    
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT = kT[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var div = WEo / AEo;

                    // For calculating WEc in Algorithm 2
                    var WEc = (WEu - WEe) - factor_kT * div * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    var WEu = data[i].OPT1_SO2;
                    var AEu = data[i].OPT2_SO2;
                
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT = kT[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var div = WEo / AEo;

                    // For calculating WEc in Algorithm 2
                    var WEc = (WEu - WEe) - factor_kT * div * (AEu - AEe);

                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);

        }else if(sensor_name === "NO2"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].Time_Stamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    var WEu = data[i].OPT1_NO2;
                    var AEu = data[i].OPT2_NO2;
    
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT = kT[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var div = WEo / AEo;

                    // For calculating WEc in Algorithm 2
                    var WEc = (WEu - WEe) - factor_kT * div * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    var WEu = data[i].OPT1_NO2;
                    var AEu = data[i].OPT2_NO2;
                
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT = kT[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var div = WEo / AEo;

                    // For calculating WEc in Algorithm 2
                    var WEc = (WEu - WEe) - factor_kT * div * (AEu - AEe);

                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);
            // res.status(200).send({"Message" : "No2 is selected"});
        }else{
            res.status(200).send({"Message" : `This sensor ${sensor_name} is not available right now!`});
        }

    }
    // If Algorithm 3
    else if(algo === 3){
        var values = [];

        var WET = JSON.parse(table8.WE_Zero_WET);
        var AET = JSON.parse(table8.Aux_Zero_AET);

        var obj = {
            "sensor_name" : sensor_name,
            values
        };

        if(sensor_name === "SO2"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].Time_Stamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    var WEu = data[i].OPT1_SO2;
                    var AEu = data[i].OPT2_SO2;
    
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT1 = kT1[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var diff = WEo - AEo;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    var WEu = data[i].OPT1_SO2;
                    var AEu = data[i].OPT2_SO2;
                
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT1 = kT1[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var diff = WEo - AEo;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);

        }else if(sensor_name === "NO2"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].Time_Stamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    var WEu = data[i].OPT1_NO2;
                    var AEu = data[i].OPT2_NO2;
    
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT1 = kT1[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var diff = WEo - AEo;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    var WEu = data[i].OPT1_NO2;
                    var AEu = data[i].OPT2_NO2;
                
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT1 = kT1[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var diff = WEo - AEo;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);

                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);
            // res.status(200).send({"Message" : "No2 is selected"});
        }else{
            res.status(200).send({"Message" : `This sensor ${sensor_name} is not available right now!`});
        }
        // console.log("Algo 3 is selected");
    }
    // If Algorithm 4
    else if(algo === 4){
        var values = [];

        var WET = JSON.parse(table8.WE_Zero_WET);
        var AET = JSON.parse(table8.Aux_Zero_AET);

        var obj = {
            "sensor_name" : sensor_name,
            values
        };

        if(sensor_name === "SO2"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].Time_Stamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    var WEu = data[i].OPT1_SO2;
                    var AEu = data[i].OPT2_SO2;
    
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT2 = kT2[`${temp}`];

                    var WEo = WET - WEe;
                    // var AEo = AET - AEe;

                    // For calculating WEc using Algorithm 4
                    var WEc = (WEu - WEe) - WEo - factor_kT2;
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    var WEu = data[i].OPT1_SO2;
                    var AEu = data[i].OPT2_SO2;
                
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT2 = kT2[`${temp}`];

                    var WEo = WET - WEe;
                    // var AEo = AET - AEe;

                    var WEc = (WEu - WEe) - WEo - factor_kT2;

                    // For calculating WEc in Algorithm 4

                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);

        }else if(sensor_name === "NO2"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].Time_Stamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    var WEu = data[i].OPT1_NO2;
                    var AEu = data[i].OPT2_NO2;
    
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT2 = kT2[`${temp}`];

                    var WEo = WET - WEe;
                    // var AEo = AET - AEe;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - WEo - factor_kT2;
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    var WEu = data[i].OPT1_NO2;
                    var AEu = data[i].OPT2_NO2;
                
                    var temp = data[i].Temp_in_oC;
                    temp = JSON.parse(temp.toFixed(1));
    
                    var factor_kT2 = kT2[`${temp}`];

                    var WEo = WET - WEe;
                    // var AEo = AET - AEe;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - WEo - factor_kT2;
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / table8.Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * table8.Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);

        }else{
            res.status(200).send({"Message" : `This sensor ${sensor_name} is not available right now!`});
        }
        // console.log("Algo 4 is selected");
    }
    // If any of the above algorithm is not selected
    else{
        res.status(200).send({"Message" : "Please select an algorithm"});
    }

})

router.post('/4.0', async function (req, res){
    var algo = req.body.algorithm;
    var sensor_name = req.body.sensor_name;
    let dev_id = req.body.dev_id;
    var sensor_number = req.body.sensor_number;
    var from_date = moment(req.body.from_date, 'YYYY-MM-DD HH:mm:ss');
    var to_date = moment(req.body.to_date, 'YYYY-MM-DD HH:mm:ss');

    // constants
    var WEe = req.body.Electronic_Zero_WEe;
    var AEe = req.body.Electronic_Zero_AEe;

    var WET = req.body.WE_Zero_WET;
    var AET = req.body.Aux_Zero_AET;

    // console.log({WEe, AEe, WET, AET});

    // var WEe = table8.Electronic_Zero_WEe;
    // var AEe = table8.Electronic_Zero_AEe;

     //let dev_id = `02bc1a6ba121b3346a058bb72f0be4b8`;

    for(var i = 0; i < table9.length; i++){
        if(dev_id === table9[i].Assigned_Dev_ID && sensor_number === table9[i].Sensor_Number && sensor_name === table9[i].Sensor_Type && WEe === table9[i].ELECTRONIC_ZERO_WEe && AEe === table9[i].ELECTRONIC_ZERO_AEe
            && WET === table9[i].WE_Zero_WET && AET === table9[i].Aux_Zero_WET){
            // console.log("YES");
            var Gain = table9[i].Gain;
            var Sensitivity = table9[i].Sensitivity;

            let bodyData = `dev_id=${dev_id}&start_time=${from_date}&end_time=${to_date}`;
            console.log('data body : ',bodyData);
            let res_data = await axios.post('http://paribesh.distronix.in:4000/v1.0/sens/get_all_data', bodyData,  {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
            // console.log('=====>>>>>Data : ',res_data.data);
            var data = res_data.data.data;
        }
    }
    if(data === undefined){
        // console.log("Yes");
        return res.send({"Message" : "Please check the values of constants..."});
    }
    // console.log({data : data.length});
    // console.log({Gain, Sensitivity});
    
    
     
    // let bodyData = `dev_id=${dev_id}&start_time=${from_date}&end_time=${to_date}`;
    // console.log('data body : ',bodyData);
    // let res_data = await axios.post('http://paribesh.distronix.in:4000/v1.0/sens/get_all_data', bodyData,  {
    //     headers: {
    //         'Content-Type': 'application/x-www-form-urlencoded'
    //     }
    // });
    // console.log('=====>>>>>Data : ',res_data.data);
    // var data = res_data.data.data;



    // If Algorithm 1 r
    if(algo === 1){
        var values = [];

        var obj = {
            "sensor_name" : sensor_name,
            values
        };

        if(sensor_name === "SO2-B4"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].timestamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    //var WEu = data[i].OPT1_SO2;
                    var WEu = (data[i].adc3)*3;
                    //var AEu = data[i].OPT2_SO2;
                    var AEu = (data[i].adc2)*3;
    


                    var temp = parseFloat(data[i].ext_temp);
                    
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    
                    //temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);

                    

                    //temp = 0.0;
                    
    
                    var factor_nT = nT[`${temp}`];
    
                    // // For calculating WEc
                    var WEc = (WEu - WEe) - factor_nT * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    // var WEu = data[i].OPT1_SO2;
                    var WEu = (data[i].adc3)*3;

                    // var AEu = data[i].OPT2_SO2;
                    var AEu = (data[i].adc2)*3;
                
                    var temp = parseFloat(data[i].ext_temp);
                    //console.log('temp ==>>> ',typeof temp);
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        //console.log('Not eqal null fire');
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }

                    
                    // temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
                    console.log('Temp : ',temp);
                    //set static temp
                    //temp = "0.0";
    
                    var factor_nT = nT[`${temp}`];
    
                    console.log('factor_nT : ',factor_nT);
                    // // For calculating WEc using Algorithm 1
                    var WEc = (WEu - WEe) - factor_nT * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);
            
        }else if(sensor_name === "NO2-B43F"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].timestamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    // var WEu = data[i].OPT1_NO2;
                    // var AEu = data[i].OPT2_NO2;

                    var WEu = (data[i].adc4)*3;
                    var AEu = (data[i].adc1)*3;

                    var temp = parseFloat(data[i].ext_temp);
    
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    // temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_nT = nT[`${temp}`];
    
                    // // For calculating WEc
                    var WEc = (WEu - WEe) - factor_nT * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    // var WEu = data[i].OPT1_NO2;
                    // var AEu = data[i].OPT2_NO2;

                    var WEu = (data[i].adc4)*3;
                    var AEu = (data[i].adc1)*3;

                    var temp = parseFloat(data[i].ext_temp);
                
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        console.log('NOt Equal condition check')
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    temp = temp.toFixed(1);
                    //temp = JSON.parse(temp.toFixed(1));

                    
    
                    var factor_nT = nT[`${temp}`];
    
                    // // For calculating WEc using Algorithm 1
                    var WEc = (WEu - WEe) - factor_nT * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    if(WEc_obj.id === 2993847)
                    {
                        console.log('temp value === > ',temp);
                        console.log('factor_nT === >',factor_nT);
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);
            
        }else{
            res.status(200).send({"Message" : `This sensor ${sensor_name} is not available right now!`});
        }
    }
    // If Algorithm 2
    else if(algo === 2){
        // console.log("Algo 2 is selected");
        var values = [];

        // var WET = JSON.parse(table8.WE_Zero_WET);
        // var AET = JSON.parse(table8.Aux_Zero_AET);

        var obj = {
            "sensor_name" : sensor_name,
            values
        };

        if(sensor_name === "SO2-B4"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].timestamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    // var WEu = data[i].OPT1_SO2;
                    var WEu = (data[i].adc3)*3;

                    // var AEu = data[i].OPT2_SO2;
                    var AEu = (data[i].adc2)*3;

                    var temp = parseFloat(data[i].ext_temp);
    
                    //var temp = data[i].Temp_in_oC;
                    if(data[i.adc0] != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    // temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_kT = kT[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var div = WEo / AEo;

                    // For calculating WEc in Algorithm 2
                    var WEc = (WEu - WEe) - factor_kT * div * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    // var WEu = data[i].OPT1_SO2;
                    var WEu = (data[i].adc3)*3;

                    // var AEu = data[i].OPT2_SO2;
                    var AEu = (data[i].adc2)*3;

                    var temp = parseFloat(data[i].ext_temp);
                
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    //temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_kT = kT[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var div = WEo / AEo;

                    // For calculating WEc in Algorithm 2
                    var WEc = (WEu - WEe) - factor_kT * div * (AEu - AEe);

                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);

        }else if(sensor_name === "NO2-B43F"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].timestamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    // var WEu = data[i].OPT1_NO2;
                    // var AEu = data[i].OPT2_NO2;
    
                    var WEu = (data[i].adc4)*3;
                    var AEu = (data[i].adc1)*3;

                    var temp = parseFloat(data[i].ext_temp);
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    // temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_kT = kT[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var div = WEo / AEo;

                    // For calculating WEc in Algorithm 2
                    var WEc = (WEu - WEe) - factor_kT * div * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    // var WEu = data[i].OPT1_NO2;
                    // var AEu = data[i].OPT2_NO2;

                    var WEu = (data[i].adc4)*3;
                    var AEu = (data[i].adc1)*3;

                    var temp = parseFloat(data[i].ext_temp);
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    //temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_kT = kT[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var div = WEo / AEo;

                    // For calculating WEc in Algorithm 2
                    var WEc = (WEu - WEe) - factor_kT * div * (AEu - AEe);

                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);
            // res.status(200).send({"Message" : "No2 is selected"});
        }else{
            res.status(200).send({"Message" : `This sensor ${sensor_name} is not available right now!`});
        }

    }
    // If Algorithm 3
    else if(algo === 3){
        var values = [];

        // var WET = JSON.parse(table8.WE_Zero_WET);
        // var AET = JSON.parse(table8.Aux_Zero_AET);

        var obj = {
            "sensor_name" : sensor_name,
            values
        };

        if(sensor_name === "SO2-B4"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].timestamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    // var WEu = data[i].OPT1_SO2;
                    var WEu = (data[i].adc3)*3;

                    // var AEu = data[i].OPT2_SO2;
                    var AEu = (data[i].adc2)*3;
    
                    var temp = parseFloat(data[i].ext_temp);
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    // temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_kT1 = kT1[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var diff = WEo - AEo;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    // var WEu = data[i].OPT1_SO2;
                    var WEu = (data[i].adc3)*3;

                    // var AEu = data[i].OPT2_SO2;
                    var AEu = (data[i].adc2)*3;
                
                    var temp = parseFloat(data[i].ext_temp);
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    //temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_kT1 = kT1[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var diff = WEo - AEo;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);

        }else if(sensor_name === "NO2-B43F"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].timestamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    // var WEu = data[i].OPT1_NO2;
                    // var AEu = data[i].OPT2_NO2;
    
                    var WEu = (data[i].adc4)*3;
                    var AEu = (data[i].adc1)*3;

                    var temp = parseFloat(data[i].ext_temp);
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    // temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_kT1 = kT1[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var diff = WEo - AEo;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    // var WEu = data[i].OPT1_NO2;
                    // var AEu = data[i].OPT2_NO2;

                    var WEu = (data[i].adc4)*3;
                    var AEu = (data[i].adc1)*3;

                    var temp = parseFloat(data[i].ext_temp);
                
                    //var temp = data[i].Temp_in_oC;
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    //temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);
    
                    var factor_kT1 = kT1[`${temp}`];

                    var WEo = WET - WEe;
                    var AEo = AET - AEe;
    
                    var diff = WEo - AEo;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - (diff) - factor_kT1 * (AEu - AEe);

                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);
            // res.status(200).send({"Message" : "No2 is selected"});
        }else{
            res.status(200).send({"Message" : `This sensor ${sensor_name} is not available right now!`});
        }
        // console.log("Algo 3 is selected");
    }
    // If Algorithm 4
    else if(algo === 4){
        var values = [];

        // var WET = JSON.parse(table8.WE_Zero_WET);
        // var AET = JSON.parse(table8.Aux_Zero_AET);

        var obj = {
            "sensor_name" : sensor_name,
            values
        };

        if(sensor_name === "SO2-B4"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].timestamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    // var WEu = data[i].OPT1_SO2;
                    var WEu = (data[i].adc3)*3;

                    // var AEu = data[i].OPT2_SO2;
                    var AEu = (data[i].adc2)*3;

                    //var temp = data[i].Temp_in_oC;

                    var temp = parseFloat(data[i].ext_temp);
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }

                    // temp = JSON.parse(temp.toFixed(1));

                    temp = temp.toFixed(1);
    
                    var factor_kT2 = kT2[`${temp}`];

                    var WEo = WET - WEe;
                    // var AEo = AET - AEe;

                    // For calculating WEc using Algorithm 4
                    var WEc = (WEu - WEe) - WEo - factor_kT2;
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    // var WEu = data[i].OPT1_SO2;
                    var WEu = (data[i].adc3)*3;

                    // var AEu = data[i].OPT2_SO2;
                    var AEu = (data[i].adc2)*3;
                
                    //var temp = data[i].Temp_in_oC;
                    var temp = parseFloat(data[i].ext_temp);
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    //temp = JSON.parse(temp.toFixed(1));

                    temp = temp.toFixed(1);
    
                    var factor_kT2 = kT2[`${temp}`];

                    var WEo = WET - WEe;
                    // var AEo = AET - AEe;

                    var WEc = (WEu - WEe) - WEo - factor_kT2;

                    // For calculating WEc in Algorithm 4

                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_SO2" : WEc,
                        "PPB_SO2" : ppb,
                        "Gain_Value_SO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);

        }else if(sensor_name === "NO2-B43F"){

            for(var i = 0; i < data.length; i++){
                var time_stamp = data[i].timestamp;
                // console.log(time_stamp);
                time_stamp = moment(time_stamp, 'YYYY-MM-DD HH:mm:ss');

                if(req.body.from_date === "" && req.body.to_date === ""){
                    // var WEu = data[i].OPT1_NO2;
                    // var AEu = data[i].OPT2_NO2;

                    var WEu = (data[i].adc4)*3;
                    AEu = (data[i].adc1)*3;
    
                    //var temp = data[i].Temp_in_oC;
                    var temp = parseFloat(data[i].ext_temp);
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    // temp = JSON.parse(temp.toFixed(1));
                    temp = temp.toFixed(1);

                    var factor_kT2 = kT2[`${temp}`];

                    var WEo = WET - WEe;
                    // var AEo = AET - AEe;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - WEo - factor_kT2;
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }

                if(time_stamp >= from_date && time_stamp <= to_date) {

                    // var WEu = data[i].OPT1_NO2;
                    // var AEu = data[i].OPT2_NO2;

                    var WEu = (data[i].adc4)*3;
                    var AEu = (data[i].adc1)*3;

                
                    //var temp = data[i].Temp_in_oC;
                    var temp = parseFloat(data[i].ext_temp);
                    if(data[i].adc0 != null)
                    {
                        temp = ((data[i].adc0*2996/1024)-500)/10;
                    }
                    // temp = JSON.parse(temp.toFixed(1));

                    temp = temp.toFixed(1);
    
                    var factor_kT2 = kT2[`${temp}`];

                    var WEo = WET - WEe;
                    // var AEo = AET - AEe;

                    // For calculating WEc in Algorithm 3
                    var WEc = (WEu - WEe) - WEo - factor_kT2;
                    WEc = parseFloat(WEc.toFixed(3));
    
                    var ppb = (WEc / Sensitivity);
                    ppb = ppb.toFixed(3)
    
                    var gain = (WEc * Gain);
                    gain = parseFloat(gain.toFixed(3));
    
                    var WEc_obj = {
                        ...data[i],
                        "WEc_NO2" : WEc,
                        "PPB_NO2" : ppb,
                        "Gain_Value_NO2" : gain
                    }
                    values.push(WEc_obj);
                }
            }
            res.status(200).send(obj);

        }else{
            res.status(200).send({"Message" : `This sensor ${sensor_name} is not available right now!`});
        }
        // console.log("Algo 4 is selected");
    }
    // If any of the above algorithm is not selected
    else{
        res.status(200).send({"Message" : "Please select an algorithm"});
    }
})

module.exports = router;