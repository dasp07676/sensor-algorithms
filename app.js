const express = require("express");
const bodyParser = require("body-parser");

const app = express();

const cors = require("cors");
var corsOptions = {
  origin: "*"
};

app.use(cors(corsOptions));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//api routes
var algo1 = require('./routes/algo1');
var algo2 = require('./routes/algo2');
var algo3 = require('./routes/algo3');
var algo4 = require('./routes/algo4');
var algo = require('./routes/algo');


app.use('/algorithm1', algo1);
app.use('/algorithm2', algo2);
app.use('/algorithm3', algo3);
app.use('/algorithm4', algo4);
app.use('/algorithm', algo);


app.get("/", (req, res) => {
  res.json({ message: "Welcome to Api Application." });
});

// set port, listen for requests
const PORT = process.env.PORT || 8000;
app.listen(PORT, '0.0.0.0', () => {
  console.log(`Server is running on port ${PORT}.`);
});
